Decals = {
    --
    ['9090'] = {
        type = 'Albedo',
        name0 = '/maps/kaali_corner.v0001/env/decals/map_albedo.dds',
        name1 = '',
        scale = { 256, 256, 256 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },

    --
    ['9089'] = {
        type = 'Albedo',
        name0 = '/maps/kaali_corner.v0001/env/decals/map_lighting.dds',
        name1 = '',
        scale = { 256, 256, 256 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },

    --
    ['9091'] = {
        type = 'Normals',
        name0 = '/maps/kaali_corner.v0001/env/decals/map_normals.dds',
        name1 = '',
        scale = { 256, 256, 256 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
}
