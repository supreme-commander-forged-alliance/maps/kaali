Decals = {
    --
    ['9090'] = {
        type = 'Albedo',
        name0 = '/maps/kaali/env/decals/frame_18.dds',
        name1 = '',
        scale = { 512, 512, 512 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
}
