version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "kaali_core",
    description = "",
    preview = '',
    map_version = 1,
    type = 'skirmish',
    starts = true,
    size = {512, 512},
    reclaim = {0, 0},
    map = '/maps/kaali_core.v0001/kaali_core.scmap',
    save = '/maps/kaali_core.v0001/kaali_core_save.lua',
    script = '/maps/kaali_core.v0001/kaali_core_script.lua',
    norushradius = 40,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2', 'ARMY_3', 'ARMY_4'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN' ),
            },
        },
    },
}
