version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Kaali",
    description = "A beautiful reservoir on the verge of corruption after a meteoroid introduced an alien pathogen.\n\nMap is made by (Jip) Willem Wijnia. \n\nAll textures inside /env/layers are from www.textures.com. \n\nLicensed with CC BY-NC-ND 4.0.",
    preview = '',
    map_version = 4,
    type = 'skirmish',
    starts = true,
    size = {512, 512},
    reclaim = {22326.17, 70806.5},
    map = '/maps/kaali.v0004/kaali.scmap',
    save = '/maps/kaali.v0004/kaali_save.lua',
    script = '/maps/kaali.v0004/kaali_script.lua',
    norushradius = 0,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2', 'ARMY_3', 'ARMY_4'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN' ),
            },
        },
    },
}
