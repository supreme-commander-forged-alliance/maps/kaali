version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Kaali_Corner",
    description = "",
    preview = '',
    map_version = 1,
    type = 'skirmish',
    starts = true,
    size = {256, 256},
    reclaim = {0, 0},
    map = '/maps/Kaali_Corner.v0001/Kaali_Corner.scmap',
    save = '/maps/Kaali_Corner.v0001/Kaali_Corner_save.lua',
    script = '/maps/Kaali_Corner.v0001/Kaali_Corner_script.lua',
    norushradius = 40,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN' ),
            },
        },
    },
}
