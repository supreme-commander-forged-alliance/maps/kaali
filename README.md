## Kaali

A map made in collaboration with Archsimkat for the [Sunlight Mapping Tournament](https://forum.faforever.com/topic/1251/sunlight-mapping-tournament-7-2v2-10km).

<todo>

## Props

Props are edited game files of the game Supreme Commander: Forged Alliance.

## Stratum layers

All stratum textures (env/layers) are from www.textures.com. Please take note of their [license](https://www.textures.com/support/terms-of-use).

## License

The stratum layers (/env/layers) are from www.textures.com. I am obligated to add this text:
_One or more textures bundled with this project have been created with images from Textures.com. These images may not be redistributed by default. Please visit www.textures.com for more information._

All assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).